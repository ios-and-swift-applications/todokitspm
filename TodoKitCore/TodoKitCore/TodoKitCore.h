//
//  TodoKitCore.h
//  TodoKitCore
//
//  Created by Jorge luis Menco Jaraba on 24/10/19.
//  Copyright © 2019 Jorge luis Menco Jaraba. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for TodoKitCore.
FOUNDATION_EXPORT double TodoKitCoreVersionNumber;

//! Project version string for TodoKitCore.
FOUNDATION_EXPORT const unsigned char TodoKitCoreVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <TodoKitCore/PublicHeader.h>


