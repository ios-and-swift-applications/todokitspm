//
//  ContentView.swift
//  TodoKitUI
//
//  Created by Jorge luis Menco Jaraba on 23/10/19.
//  Copyright © 2019 Jorge luis Menco Jaraba. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello World")
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
